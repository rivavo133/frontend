import { Provider } from "mobx-react";
import React, { PureComponent } from "react";

import { IStores } from "../CApp";
import { CAlbumStore } from "../stores/CAlbumStore";
import { CFileStore } from "../stores/CFileStore";
import spreadFile from "../utils/spreadFile";
import CAlbum from "./CAlbum";
import CAPI from "./CAPI";
import CFeatures from "./CFeatures";
import CHome from "./CHome";
import CNotFound from "./CNotFound";
import CPlayer from "./CPlayer";
import CPrivacy from "./CPrivacy";
import CRemoval from "./CRemoval";
import CStatus from "./CStatus";
import CTerms from "./CTerms";

const fileUploadStore: IStores = {
  album: new CAlbumStore(),
  file: new CFileStore()
};

const textUploadStore: IStores = {
  album: new CAlbumStore(),
  file: new CFileStore()
};

const createAlbumStore: IStores = {
  album: new CAlbumStore(),
  file: new CFileStore()
};

const loadedAlbumStoreGrid: IStores = {
  album: new CAlbumStore(),
  file: new CFileStore()
};

const loadedAlbumStoreLight: IStores = {
  album: new CAlbumStore(),
  file: new CFileStore()
};

const file = spreadFile(
  new File([new Blob(["body"], { type: "text/plain" })], "title"),
  {
    percent: 100,
    response: {
      status: "success",
      data: {
        deleteLink: "https://api.put.re/delete/test.png/deltoken",
        deleteToken: "deltoken",
        extension: ".png",
        link: "https://s.put.re/test.png",
        name: "muhname.png",
        originalName: "muhOriginalName",
        size: 1337
      }
    },
    status: "done"
  }
);

fileUploadStore.file.upsertEntry({ ...file, ...{ percent: 50 } });

for (let index = 2; index < 100; index++) {
  fileUploadStore.file.upsertEntry(file);
}

textUploadStore.file.toggleUploader();
textUploadStore.file.toggleModal(0);

createAlbumStore.album.isCreated = true;

const albumFiles = [];
for (let index = 0; index < 11; index++) {
  albumFiles.push({
    createdAt: Date.now(),
    description: "File1 Description",
    fileId: "tejaTJt5.png",
    title: "File1 title",
    updatedAt: Date.now(),
    dimensions: {
      width: 100,
      height: 100
    }
  });
}

loadedAlbumStoreLight.album.album = {
  createdAt: Date.now(),
  description: "Album Description",
  entries: albumFiles,
  title: "Album title",
  updatedAt: Date.now()
};

loadedAlbumStoreLight.album.dirty = true;

loadedAlbumStoreGrid.album.album = {
  createdAt: Date.now(),
  description: "Album Description",
  entries: albumFiles,
  title: "Album title",
  updatedAt: Date.now()
};

loadedAlbumStoreGrid.album.toggleView();
loadedAlbumStoreGrid.album._canEdit = true;
loadedAlbumStoreGrid.album.selectedEntries = [0];

const stores = [
  loadedAlbumStoreLight,
  loadedAlbumStoreGrid,
  createAlbumStore,
  textUploadStore,
  fileUploadStore
];

export default class CPurgeCSS extends PureComponent {
  render() {
    return (
      <>
        {stores.map(store => (
          <Provider stores={store}>
            <CAlbum
              history={{} as any}
              location={{ pathname: "/album" } as any}
              match={{ params: { albumId: store.album.albumId } } as any}
            />
          </Provider>
        ))}
        {stores.map(store => (
          <Provider stores={store}>
            <CHome />
          </Provider>
        ))}
        <CRemoval urls="error" validateOnLoad />
        <CRemoval />
        <CFeatures />
        <CPrivacy />
        <CAPI activePage="1" />
        <CAPI activePage="2" />
        <CAPI activePage="3" />
        <CStatus state={{}} />
        <CStatus
          state={{
            data: {
              uptime: 1,
              load: [0, 0, 0],
              services: [
                { name: "PurgeCSS", status: "OK", ping: 1 },
                { name: "PurgeCSS2", status: "Error down", ping: 1 }
              ]
            }
          }}
        />
        <CStatus
          state={{
            data: {
              uptime: 1,
              load: [0, 0, 0],
              services: [{ name: "PurgeCSS", status: "OK", ping: 1 }]
            }
          }}
        />
        <CTerms />
        <CNotFound
          history={{} as any}
          location={{ pathname: "/blah" } as any}
          match={{} as any}
        />
        <CAlbum
          history={{} as any}
          location={{ pathname: "/album" } as any}
          match={{ params: { albumId: null } } as any}
        />
        <CPlayer
          history={{} as any}
          location={{} as any}
          match={{ params: {} } as any}
          state={{ originalName: "name.mp4", url: "url.mp4" }}
        />
      </>
    );
  }
}
