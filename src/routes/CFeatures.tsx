import { Col, Divider, Row, Typography } from "antd";
import React, { PureComponent } from "react";
import { Link } from "react-router-dom";

import CCode from "../partials/app/CCode";

const { Title, Paragraph, Text } = Typography;

export default class CFeatures extends PureComponent {
  render() {
    return (
      <>
        <Row>
          <Col>
            <Title>Features</Title>
          </Col>
        </Row>

        <Row>
          <Col>
            <Divider />
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>ShareX support</Title>
            <Paragraph>
              Click anywhere below to copy our configuration and import it under{" "}
              <Text type="secondary">Destination Settings</Text> in{" "}
              <a href="https://github.com/ShareX/ShareX">ShareX</a> to directly
              upload your files to put.re. If you need assistance we would
              recommend checking out our{" "}
              <Link to="/player/CRAqeja1.mp4">video guide</Link>.
            </Paragraph>

            <CCode
              mode="application/json"
              value={JSON.stringify(
                {
                  Name: "put.re",
                  LegacyConfiguration: true,
                  RequestType: "POST",
                  RequestURL: "https://api.put.re/upload",
                  FileFormName: "file",
                  ResponseType: "Text",
                  URL: "$json:data.link$",
                  ThumbnailURL: "$json:data.thumbnailLink$",
                  DeletionURL: "$json:data.deleteLink$"
                },
                null,
                2
              )}
            />

            <Paragraph />
          </Col>
        </Row>
        <Row>
          <Col>
            <Title level={2}>All files are supported</Title>
            <Paragraph>
              There is no file type restriction. We are detecting the mimetype
              from the{" "}
              <a href="https://en.wikipedia.org/wiki/List_of_file_signatures">
                magic bytes
              </a>{" "}
              marking each file &mdash; therefore put.re uses the actual file to
              recognize the type and not the extension. If it is an unknown
              mimetype the file will be served as{" "}
              <Text type="secondary">application/octet-stream</Text>. Please be
              aware that put.re is forcing certain mimetypes (for example HTML
              or JavaScript) to be served as{" "}
              <Text type="secondary">text/plain</Text>. This prevents the
              browser from executing files.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Unlimited retention</Title>
            <Paragraph>
              We do not delete any files unless you ask us to or they violate
              our <Link to="/terms-of-service">terms of service</Link>.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Reasonable file size limits</Title>
            <Paragraph>
              The web interface supports file sizes up to{" "}
              <Text strong>200 MB</Text> and our API <Text strong>100 MB</Text>.
              These limits are different because the web uploader is making use
              of{" "}
              <a href="https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API">
                WebSockets
              </a>{" "}
              for bigger files, which also results in the web interface being
              faster for big files.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>No compression</Title>
            <Paragraph>
              All files are served exactly how they are provided &mdash; this is
              known as "MD5 perfect". A lot of other image hosters apply
              compression to your file, which results in a smaller file size to
              store. Even if there can be no visual difference we disapprove of
              this method, because it makes it impossible to share high quality
              source files.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Fast access</Title>
            <Paragraph>
              All files are served with a multi-regional CDN (content delivery
              network) which ensures fast access from all over the world. Added
              to this everything is cached directly on the edge of the network
              to reduce latency and avoid unnecessary requests to the origin
              servers.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Direct links</Title>
            <Paragraph>
              You receive the direct link for every file in every case.
              Uploading a video file in the web uploader is an exception and
              returns a link to our player page. You may still opt out and use
              the direct link provided by the API and on the player page.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Cloud storage</Title>
            <Paragraph>
              Your files are stored encrypted in an Amazon AWS S3 bucket &mdash;
              without Amazon being able to read the files. At no point in the
              processing chain your file is written to a disk unencrypted. Using
              the delete link provided with each upload will physically delete
              the file from all storages and caches. This is done instead of
              soft deleting (marking a file "deleted" and preventing access to
              it), which sadly became the industry standard for any kind of
              data.
            </Paragraph>
          </Col>
        </Row>

        <Row>
          <Col>
            <Title level={2}>Truly free</Title>
            <Paragraph>
              If something is "free" the user is usually the product. However,
              put.re is not generating any revenue: We are fortunate enough to
              have other well paying projects funding put.re. We offer this
              service because we are privacy enthusiasts and none other public
              services met our demands. Please refer to our{" "}
              <Link to="/privacy">privacy policy</Link> to learn exactly how
              your data is processed. Additionally put.re is designed to be as
              cost efficient as possible &mdash; it is probably more expensive
              to run a few Wordpress blogs. We have a huge personal motivation
              to keep this service running ourselves, thus put.re will be there
              for years to come.
            </Paragraph>
          </Col>
        </Row>
      </>
    );
  }
}
